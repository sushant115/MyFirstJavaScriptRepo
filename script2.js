var circles =[];

function setup(){
	createCanvas(600,400);
	
	for(let a=0;a<200;a++){
	 circles[a] =new Cir(random(width),random(height));
		
	}

}
function draw(){
	background(0);
  for(let a=0;a<circles.length;a++){
  	
  	circles[a].update();
  	circles[a].show();
  }
  
}